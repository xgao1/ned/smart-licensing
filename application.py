from flask import Flask, render_template, request, jsonify
import os
import requests
from datetime import date, timedelta, datetime
import netdev
import asyncio
import jtextfsm as textfsm


application = app = Flask(__name__)

CLIENT_ID = os.environ['CLIENT_ID']
CLIENT_SECRET = os.environ['CLIENT_SECRET']
SA_USERNAME = os.environ['SA_USERNAME']    
SA_PASSWORD = os.environ['SA_PASSWORD']
SA_ALIAS = os.environ['SA_ALIAS']
AAA_USERNAME = os.environ['AAA_USERNAME']
AAA_PASSWORD = os.environ['AAA_PASSWORD']

R_TOKEN_ON = None       # registration token for 'On Net CPE'
# date of the on-net registration token getting expired <class 'datetime.date'>
RTO_EXPIRE_DATE = None

# custom message excpetion
class Custom_Exp(Exception):
    def __self__(self, msg):
        self.msg = msg

    def __repr__(self):
        return self.msg


# get api access token to cisco.com for 'Off Net CPE'
def get_acc_token_to_cisco_com():
    url = 'https://cloudsso.cisco.com/as/token.oauth2'
    headers = {
        'content-type': 'application/x-www-form-urlencoded'
    }
    payload = {
        'grant_type': 'password',
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'username': SA_USERNAME,
        'password': SA_PASSWORD
    }
    resp = requests.post(url=url, headers=headers, data=payload)
    if resp.status_code == 200:  # when successfully fetching an access token
        return resp.json()['access_token']
    # when any of the four credentials was incorrectly provided
    elif resp.status_code == 400 or resp.status_code == 401:
        raise Exception(resp.json()['error_description'])
    else:
        raise Custom_Exp(f'Request failed on {url}')


# list all available registration tokens on 'Off Net CPE' virtual account
def list_ava_r_tokens_off(sa_domain, va_name, acc_token):
    url = f'https://swapi.cisco.com/services/api/smart-accounts-and-licensing/v1/accounts/{sa_domain}/virtual-accounts/{va_name}/tokens'
    headers = {
        'authorization': f'Bearer {acc_token}'
    }
    resp = requests.get(url=url, headers=headers)
    if resp.status_code == 200:
        r_tokens = []
        tokens_info = resp.json()['tokens']
        if tokens_info:
            for item in tokens_info:
                if item['createdBy'] == SA_ALIAS:
                    r_tokens += [(item['token'])]
        return r_tokens
    else:
        raise Custom_Exp(f'Request failed on {url}')


# create a new registration token on 'Off Net CPE' virtual account
def create_new_r_token_off(sa_domain, va_name, acc_token):
    url = f'https://swapi.cisco.com/services/api/smart-accounts-and-licensing/v1/accounts/{sa_domain}/virtual-accounts/{va_name}/tokens'
    headers = {
        'authorization': f'Bearer {acc_token}',
        'content-type': 'application/json'
    }
    payload = {
        'description': 'Token created by NED',
        'expiresAfterDays': 30,
        'exportControlled': 'Allowed'
    }
    resp = requests.post(url=url, headers=headers, json=payload)
    if resp.status_code == 200:
        return resp.json()['tokenInfo']['token']
    else:
        raise Custom_Exp(f'Request failed on {url}')


# extract license status information from the output of 'show license status' command
def extract_lice_info_from_out(sh_lic_st_out):
    with open('textfsm/show_license_status.textfsm') as parsing_template:
        re_table = textfsm.TextFSM(parsing_template)
    out_parsed = re_table.ParseText(sh_lic_st_out)
    lice_info = {
        'status': out_parsed[0][0],
        'smart_account': out_parsed[0][1],
        'virtual_account': out_parsed[0][2],
        'initial_registration': out_parsed[0][3]
    }
    print(lice_info)
    return lice_info


# register a cpe as off-net
async def register_cpe_off(cpe_param, r_token):
    try:
        async with netdev.create(**cpe_param) as cpe:
            await cpe.send_command("configure terminal")
            await cpe.send_command("ip domain-lookup")
            await cpe.send_command("ip name-server 8.8.8.8 4.2.2.2")
            await cpe.send_command("service call-home")
            await cpe.send_command("call-home")
            await cpe.send_command("contact-email-addr sch-smart-licensing@cisco.com")
            await cpe.send_command("profile \"CiscoTAC-1\"")
            await cpe.send_command("active")
            await cpe.send_command("destination transport-method http")
            await cpe.send_command("exit")
            await cpe.send_command("license smart enable")
            await cpe.send_command("exit")
            await cpe.send_command(f"license smart register idtoken {r_token}")

            # check license status till 'REGISTERED' status or 'REGISTRATION FAILED' statue are seen, or time is up to 60 seconds
            time_begin = datetime.now()
            while True:
                out = await cpe.send_command("show license status")
                lice_info = extract_lice_info_from_out(out)
                if lice_info['status'] == 'REGISTERED' or (datetime.now() - time_begin).seconds >= 60:
                    return cpe_param['host'], lice_info['initial_registration'], None
                if 'REGISTRATION FAILED' in lice_info['status']:
                    raise Custom_Exp(f'{cpe_param["host"]}: {lice_info["initial_registration"]}')
    except (netdev.exceptions.TimeoutError, Custom_Exp) as e:
        return cpe_param['host'], None, str(e)


# register a cpe as on-net
async def register_cpe_on(cpe_param, r_token):
    try:
        async with netdev.create(**cpe_param) as cpe:
            await cpe.send_command("configure terminal")
            await cpe.send_command("service call-home")
            await cpe.send_command("ip http client source-interface Loopback66")
            await cpe.send_command("call-home")
            await cpe.send_command("contact-email-addr sch-smart-licensing@cisco.com")
            await cpe.send_command("source-interface loop 66")
            await cpe.send_command("profile \"CiscoTAC-1\"")
            await cpe.send_command("active")
            await cpe.send_command("destination transport-method http")
            await cpe.send_command("destination address http http://198.19.62.38:80/Transportgateway/services/DeviceRequestHandler")
            await cpe.send_command("exit")
            await cpe.send_command("license smart enable")
            await cpe.send_command("exit")
            await cpe.send_command(f"license smart register idtoken {r_token}")

            # check license status till 'REGISTERED' status is seen or time is up to 60 seconds
            time_begin = datetime.now()
            while True:
                out = await cpe.send_command("show license status")
                lice_info = extract_lice_info_from_out(out)
                if lice_info['status'] == 'REGISTERED' or (datetime.now() - time_begin).seconds >= 60:
                    return cpe_param['host'], lice_info['initial_registration'], None
                if 'REGISTRATION FAILED' in lice_info['status']:
                    raise Custom_Exp(f'{cpe_param["host"]}: {lice_info["initial_registration"]}')
    except (netdev.exceptions.TimeoutError, Custom_Exp) as e:
        return cpe_param['host'], None, str(e)


# revoke smart license registration of an off-net/off-net cpe
async def revoke_regi(cpe_param):
    try:
        async with netdev.create(**cpe_param) as cpe:
            await cpe.send_command("configure terminal")
            await cpe.send_command("no license smart enable")
            return cpe_param['host'], 'SUCCEEDED', None
  
    except netdev.exceptions.TimeoutError as e:
        return cpe_param['host'], None, str(e)


@app.route('/test')
def test():
    acc_token = get_acc_token_to_cisco_com()
    a = list_ava_r_tokens_off('granitenet.com', 'Off Net CPE', acc_token)
    print(a)
    return 'good sign'


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/registerOff', methods=['POST'])
def register_off():
    req_date = request.get_json()
    ips = req_date.get('ips')
    try:
        acc_token = get_acc_token_to_cisco_com()

        # use a random registration token available on 'Off Net CPE', otherwise create a new one
        ava_r_tokens = list_ava_r_tokens_off(
            'granitenet.com', 'Off Net CPE', acc_token)
        if not ava_r_tokens:
            r_token = create_new_r_token_off(
                'granitenet.com', 'Off Net CPE', acc_token)
        else:
            r_token = ava_r_tokens[0]

        print('<--------------')
        print(f'Token Used: {r_token}')
        print('-------------->')

    except Custom_Exp as e:   # catch exception caused by: no api call support from cisco.com
        return jsonify({
            'status': 'ERROR',
            'error': 'API call to cisco.com failed',
            'message': str(e)
        })

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    tasks = []
    for ip in ips:
        cpe = {
            'username': AAA_USERNAME,
            'password': AAA_PASSWORD,
            'device_type': 'cisco_ios',
            'host': ip
        }
        tasks += [register_cpe_off(cpe, r_token)]
    results = loop.run_until_complete(asyncio.gather(*tasks))
    succ = []
    fail = []
    for item in results:
        if item[1] != None:
            succ += [(item[0], item[1])]
        if item[2] != None:
            fail += [(item[0], item[2])]
    return jsonify({
        'status': 'SUCCESS',
        'success': succ,
        'failure': fail
    })


@app.route('/checkOnToken', methods=['POST'])
def check_r_token_on():
    if RTO_EXPIRE_DATE == None or (RTO_EXPIRE_DATE - date.today()).days <= 0 \
            or R_TOKEN_ON == None:
        return jsonify({
            'status': 'FAILURE'
        })
    else:
        print(R_TOKEN_ON)
        return jsonify({
            'status': 'SUCCESS',
            'token': R_TOKEN_ON
        })


@app.route('/updateOnToken', methods=['POST'])
def update_r_token_on():
    global R_TOKEN_ON
    global RTO_EXPIRE_DATE

    req_date = request.get_json()
    r_token = req_date['token']
    expire_after_days = req_date['days']

    R_TOKEN_ON = r_token
    RTO_EXPIRE_DATE = date.today() + timedelta(int(expire_after_days))

    print(R_TOKEN_ON)
    return jsonify({
        'status': 'SUCCESS',
        'token': R_TOKEN_ON
    })


@app.route('/registerOn', methods=['POST'])
def register_on():
    req_date = request.get_json()
    ips = req_date.get('ips')
    r_token = req_date.get('r_token')

    print('<--------------')
    print('Token Used:', r_token)
    print('--------------->')

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    tasks = []
    for ip in ips:
        cpe = {
            'username': AAA_USERNAME,
            'password': AAA_PASSWORD,
            'device_type': 'cisco_ios',
            'host': ip
        }
        tasks += [register_cpe_on(cpe, r_token)]
    results = loop.run_until_complete(asyncio.gather(*tasks))
    succ = []
    fail = []
    for item in results:
        if item[1] != None:
            succ += [(item[0], item[1])]
        if item[2] != None:
            fail += [(item[0], item[2])]
    return jsonify({
        'status': 'SUCCESS',
        'success': succ,
        'failure': fail
    })


@app.route('/revokeOff', methods=['POST'])
@app.route('/revokeOn', methods=['POST'])
def revoke_regi_off_on():
    req_date = request.get_json()
    ips = req_date.get('ips')

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    tasks = []
    for ip in ips:
        cpe = {
            'username': AAA_USERNAME,
            'password': AAA_PASSWORD,
            'device_type': 'cisco_ios',
            'host': ip
        }
        tasks += [revoke_regi(cpe)]
    results = loop.run_until_complete(asyncio.gather(*tasks))
    succ = []
    fail = []
    for item in results:
        if item[1] != None:
            succ += [(item[0], item[1])]
        else:
            fail += [(item[0], item[2])]
    return jsonify({
        'status': 'SUCCESS',
        'success': succ,
        'failure': fail
    })


if __name__ == '__main__':
    get_acc_token_to_cisco_com()  # test if wrong credentials were provided
    app.run(debug=True, port=5001, host='0.0.0.0')
