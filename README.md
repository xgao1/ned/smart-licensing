# Small Licensing

> Smart Licensing registers an on-net CPE(Customer-Premises Equipment) to the Satellite software and an off-net CPE to the Cisco licensing software. It is capable of registering/revoking multiple pieces of CPE in one go.

## Run in Local Development

**> *Install Python3.6+ & virtualenv***

**> *Create & enter a virtual environment***

**> *Install dependencies***
  
    pip install -r requirements.txt

**> *Set environment variables(e.g. cmd)***
  
    set DEV_ENV=1
    
    # Client credentials to enable access to Cisco.com APIs
    # To request access: https://anypoint.mulesoft.com/apiplatform/apx#/portals/organizations/1c92147b-332d-4f44-8c0e-ad3997b5e06d/apis/5418104/versions/102456/pages/425744
    set CLIENT_ID=x
    set CLIENT_SECRET=x

    # On-net registration token, date of the token generated, and in days that the token expires
    set R_TOKEN_ON=x
    set DATE_RTO_GENERATED=yyyy/mm/dd
    set AFTER_DAYS_RTO_EXPIRED=x

    # Credentials to log into the Satellite software, and alias of the login account, which can be found in the user profile
    set SA_USERNAME=x
    set SA_SA_PASSWORD=x
    set SA_ALIAS=x

    # Credentials with proper write privileges to log into device management 
    set AAA_USERNAME=x            
    set AAA_PASSWORD=x         


**> *Run the app***
    
    python application.py

## API Calls

#### Validate the on-net registration token(i.e. R_TOKEN_ON) &#x25BE;

    POST: /checkOnToken

###### REQUEST PARAMETERS

    null

###### SAMPLE RESPONSES

     # When the on-net registration token is already expired
     {'status': 'FAILURE'}

     {
      'status': 'FAILURE', 
      'type': <0 or 1>,       # 0 when the token expires in less than 30 days, while 1 in more than 30 days 
      'token': R_TOKEN_ON      
     }

#### Register devices as on-net &#x25BE;

    POST: /registerOn

###### REQUEST PARAMETERS

*Content-Type: application/json*

    ips          IP addresses of devices to be licensing
    r_token      On-net registration token

#### Register devices as off-net &#x25BE;

    POST: /registerOff
    
###### REQUEST PARAMETERS

*Content-Type: application/json*

    ips          IP addresses of devices to be licensing

#### Revoke licensing of on-net/off-net devices &#x25BE;

    POST: /revoke

###### REQUEST PARAMETERS

*Content-Type: application/json*

    ips          IP addresses of devices to be revoked licensing
